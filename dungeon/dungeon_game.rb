class Dungeon
  ### DESIGNING THE GAME
  attr_accessor :player
  def initialize(player_name)
    @player = Player.new(player_name)
    @rooms = Array.new
  end
  
  # Define the attributes of Players
  class Player
    attr_accessor :name, :location
    
    def initialize(player_name)
      @name = player_name
    end
  end
  
  # Define the attributes of Rooms
  class Room
    attr_accessor :reference, :name, :description, :connections
    
    def initialize(reference, name, description, connections)
      @reference = reference
      @name = name
      @description = description
      @connections = connections
    end
    
    def full_description
      @name + "\nYou are in " + @description
    end
  end
  
  ## SETTING UP THE GAME
  
  # Creating Rooms inside the Dungeon
  def add_room(reference, name, description, connections)
    @rooms << Room.new(reference, name, description, connections)
  end

  # assign the player's start location
  def start(location)
    @player.location = location
    show_current_description
  end

  ## PLAYING THE GAME

  # find the current room in the dungeon
  def find_room_in_dungeon(reference)
    @rooms.detect { |room| room.reference == reference }
  end
  
  # get adjacent rooms
  def find_room_in_direction(direction)
    find_room_in_dungeon(@player.location).connections[direction]
  end
  
  # moving from room to room
  def go(direction)
    puts "You go #{direction}"
    @player.location = find_room_in_direction(direction)
    show_current_description
  end
  
  # staying put
  def stay(direction)
    puts "The is nothing to the #{direction}."
    show_current_description
  end
  
  # check for room
  def checking(direction)
    if (find_room_in_direction(direction)!=nil) 
      go(direction)
    else 
      stay(direction)
    end   
  end
  
  # put the current location
  def show_current_description
    puts find_room_in_dungeon(@player.location).full_description
  end
    
end # The dungeon class wraps the player and room classes because they are useless without it

### BEGINNING THE GAME

# initialize the Dungeon, creating a player
my_dungeon = Dungeon.new("Regan Leah")

# add the rooms
my_dungeon.add_room(:largecave, "Large Cave", "a large, cavernous cave", { "S" => :smallcave, "W" => :river } )
my_dungeon.add_room(:smallcave, "Small Cave", "a small, claustrophobic cave", { "N" => :largecave, "W" => :mountain } )
my_dungeon.add_room(:river, "River Bank", "the edge of a river bank", { "E" => :largecave, "S" => :mountain } )
my_dungeon.add_room(:mountain, "Mountain Foothill", "the foothills of the mountain range", { "E" => :smallcave, "N" => :river } )

# put the player in a room
my_dungeon.start(:largecave)

### PLAYING THE GAME!
begin
  desired_direction = gets.chomp
  my_dungeon.checking(desired_direction)
end while not desired_direction.empty?

